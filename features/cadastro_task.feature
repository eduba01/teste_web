#language: pt
#utf-8

@cadastro_task
Funcionalidade: Cadastro de tasks
  Eu como usuario
  Quero me cadastrar no site
  Para para criar novas tasks

Contexto: Acessar a home page
  Dado que eu esteja na home page
  Quando eu fizer o login com "usuario_valido_1"

@negativo
Cenário: Validar cadastro de tasks com erro
  Quando clicar no botão para adicionar novas tasks
  E eu criar um novo cadastro de task com "", "", "" e ""
  E eu clicar no botão save
  Então devo visualizar a mensagem de erro ""  
  # Neste caso o sistema não exibiu mensagem de erro, só critica nos campos
  # pesquisar como validar este cenário

@positivo
Cenário: Validar cadastro de tasks com sucesso
  Quando clicar no botão para adicionar novas tasks
  E eu criar um novo cadastro de task com "Task Teste01", "3 March, 2020", "10:00" e "Task01"
  E eu clicar no botão save
  Então devo visualizar a task criada "Task01"
 
 
 
 