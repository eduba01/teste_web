
Então("devo visualizar a mensagem de erro {string}") do |mensagem|
    expect(page).to have_content(mensagem)
    # msg_error
end
 
Então("devo visualizar a mensagem de boas vindas {string}") do |mensagem|
    #binding.pry
    mensagem=(MASS['USUARIO'][mensagem]['nome'])
    expect(page).to have_content(mensagem)
end

Quando("eu clicar no botão save") do
    page.find('a', text: 'SAVE').click
end 