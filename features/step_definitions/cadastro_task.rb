 
  
Dado("clicar no botão para adicionar novas tasks") do
  @cadastro_task_page.lnk_add_task.click
  @cadastro_task_page.lnk_add_new_task.click
end

Quando("eu criar um novo cadastro de task com {string}, {string}, {string} e {string}") do |title, time, date, about|
  @cadastro_task_page.do_cadastro(title, time, date, about)
end

Dado("devo visualizar a task criada {string}") do |task|
  expect(page).to have_content(task)
end