#language: pt
#utf-8

@cadastro_usuario
Funcionalidade: Cadastro de usuários
  Eu como usuario
  Quero me cadastrar no site
  Para para criar novas tasks

Contexto: Acessar a home page
  Dado que eu esteja na home page

 
@negativo
Esquema do Cenário: Validar critica de cadastro com dado invalido
  Quando eu criar um novo cadastro com <nome> e login <login> e senha <senha>
  E eu clicar no botão save
  Então devo visualizar a mensagem de erro <mensagem>

  Exemplos:
  | teste                   | nome               | login        | senha     | mensagem         |
  | "usuario já cadastrado" | "x"                | "imtlogin01" | "123456"  |"Someone choose this login before, please pick another!" | 
  | "usuario invalido"      | " "                | "imtlogin01" | "123456"  |"Someone choose this login before, please pick another!" | 
  | "senha invalida"        | "Usuário Teste055" | "imtlogin01" | " "       |"Someone choose this login before, please pick another!" | 

@positivo
Cenário: Realizar Cadastro de usuário com Sucesso
  Quando eu criar um novo cadastro com "usuario_valido_2", "nome" e login "login" e senha "senha"
  E eu clicar no botão save
  Então devo visualizar a mensagem de boas vindas "usuario_valido_2"
  