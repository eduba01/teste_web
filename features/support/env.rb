require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'site_prism'
require 'pry'

MASS = (YAML.load_file('./features/fixtures/mass.yml'))
  
  
Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host   = "http://www.juliodelima.com.br/taskit"
    config.default_max_wait_time = 10
end
 