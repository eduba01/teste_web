 
Before do
    page.current_window.resize_to(1024, 768)
    @home_page = HomePage.new
    @login_page = LoginPage.new
    @USUARIO_page = CadastroUserPage.new
    @cadastro_task_page = CadastroTaskPage.new
end

After do |scenario|
    nome_cenario = scenario.name.gsub(/[^A-Za-z0-9 ]/, '')
    nome_cenario = nome_cenario.gsub(' ', '_').downcase!
    #nome_cenario += '-' + Time.now.strftime('%d%m%Y-%H%M%S').to_s
    screenshot = "log/screenshots/#{nome_cenario}.png"
    page.save_screenshot(screenshot)
end

After do 
   #logout
end