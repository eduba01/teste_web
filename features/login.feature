#language: pt

@login
Funcionalidade: Login
  Como usuário
  Eu quero fazer login no site taskit
  Para visualizar ou criar tasks

Contexto: Acessar a home page
  Dado que eu esteja na home page
 
 
@negativo
Cenário: Fazer login erro
  Quando eu fizer o login com "usuario_invalido"
  Então devo visualizar a mensagem de erro "Maybe you brain dropped the password or login in some place!"

@positivo
Cenário: Fazer login sucesso
  Quando eu fizer o login com "usuario_valido_1"
  Então devo visualizar a mensagem de boas vindas "usuario_valido_1"

 