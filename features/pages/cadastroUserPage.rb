class CadastroUserPage < SitePrism::Page
  include Capybara::DSL
  
  #Objetos do Login 
  element :lbl_nome, :css, '#signupbox > div.modal-content > form > div:nth-child(3) > div > input'
  element :lbl_login, :css, 'div:nth-child(4) > div:nth-child(1) > input'
  element :lbl_password, :css, '#signupbox > div.modal-content > form > div:nth-child(4) > div:nth-child(2) > input'
  element :btn_save, :css, '#signupbox > div.modal-footer > a'
  element :msg_error, :css, '#toast-container > div'
  
 
  # Realiza login com usuario e senha informados
  def do_cadastro(name, login, pass)
    lbl_nome.set name
    lbl_login.set login
    lbl_password.set pass
  end


end
