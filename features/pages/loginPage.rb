class LoginPage < SitePrism::Page
  include Capybara::DSL
  
  #Objetos do Login 
  element :lbl_login, :css, '#signinbox > div.modal-content > form > div:nth-child(3) > div:nth-child(1) > input'
  element :lbl_password, :css, '#signinbox > div.modal-content > form > div:nth-child(3) > div:nth-child(2) > input'
  element :btn_signin, :css, '#signinbox > div.modal-footer > a'
 
  # Realiza login com usuario e senha informados
  def do_login(user, pass)
    @home_page = HomePage.new
    @home_page.validar_home
    @home_page.click_btn_signin
    lbl_login.set user
    lbl_password.set pass
    btn_signin.click
  end


end
