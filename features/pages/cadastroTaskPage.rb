class CadastroTaskPage < SitePrism::Page
  include Capybara::DSL
  
  #Objetos do cadastro de task
  element :lnk_add_task, :css, 'p:nth-child(3) > a'
  element :lnk_add_new_task, :css, 'div.row.center > button'

  element :lbl_title, :css, 'div:nth-child(3) > div > input'
  element :lbl_date, :css, 'input.validate.datepicker.picker__input'
  element :lbl_select_date, :css, 'tr:nth-child(1) > td:nth-child(1) > div'
  element :lbl_time, :css, 'input.validate.timepicker'
  element :lbl_select_time, :css, 'div.clockpicker-dial.clockpicker-minutes > div:nth-child(10)'
  element :lbl_about, :css, '#addtask > div.modal-content > div:nth-child(5) > div > textarea'
  element :done, :css, 'select > option:nth-child(2)'

  element :lnk_save, :css, '#addtask > div.modal-footer > a'
 
  
  # Realiza login com usuario e senha informados
  def do_cadastro(title, date, time, about)
    lbl_title.set title
    lbl_date.right_click
    lbl_select_date.click
    page.find(:css,'tr:nth-child(1) > td:nth-child(3) > div').click
    click_on 'Ok'
    lbl_time.set time
    click_on 'OK'
    lbl_about.set about
    select('Yes', from: 'done')  # Seleciona opção YES no combobox
  end


end
