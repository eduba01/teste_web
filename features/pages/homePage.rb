class HomePage < SitePrism::Page
  include Capybara::DSL

    #Objetos do Login 
    element :lnk_login, :css, 'ul.right.hide-on-med-and-down'
    element :lnk_signup, :css, '#signup'

    def load
      visit "/"
    end

    def validar_home
      wait_until_lnk_login_visible
      lnk_login.has_text?("Sign in")
    end

    def click_btn_signin
      lnk_login.click
    end

    def click_btn_signup
      lnk_signup.click
    end
   
 end

