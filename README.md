## Installation

**Segue abaixo os passos para instalação**

**1. Instalando Ruby 64 Bits**
* a) Fazer download da versão xxx x64 http://rubyinstaller.org/downloads/
* b) Instalar no diretório C:\Rubyxxx-x64
* c) Antes de clicar em Install marcar todas as opções.
* c) Feche o console, e abra novamente, em seguida, digite o comando `ruby –v`, se der tudo certo você vai ver o seguinte resultado:

>  ruby 2.xxx

**2. Instalando Devkit 64 Bits**
* a) http://dl.bintray.com/oneclick/rubyinstaller/DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe
* b) Descompactar no diretório C:\Ruby24-x64\devkit
* c) No console, acessar via linha de comando, a pasta onde foi instalado o DEVKIT.
* d) Na sequencia digite o comando:
`ruby dk.rb init`
* e) Agora abra o arquivo config.yml que foi gerado
* f) A ultima linha do arquivo deve conter o valor : -C:/Ruby24-x64. Caso contrário faça o ajuste no arquivo. Não esqueça do caractere - antes do C:\.
* g) No Console do Cmder, digite o comando: 
`ruby dk.rb install`


**3. Instalando Bundler, RSpec e Cucumber**
* Bundler: `gem install bundler`
* RSpec: `gem install rspec`
* Cucumber: `gem install cucumber`


**4. Instalando as dependências do projeto através do Bundler**
Dentro da pasta raiz do projeto executar o comando `bundler install`. 
Ao executar o comando todas as dependências devem ser instaladas. Para checar que todas foram instaladas verificar se o log bate com as dependências especificadas no arquivo Gemfile.
Caso não tenha sido instalado alguma dependência por algum motivo, é necessário realizar a instalação manualmente, caso o comando `bundler install` ainda não funcione. Para tal, verificar qual dependência faltou e executar o comando `gem install <nome_da_dependencia>`.

**5. Adicionando o chrome driver no lugar certo**
 * Realizar o download do chrome driver para windows.
 * Extrair o arquivo chromedriver.exe para o diretório: C:\Ruby24-x64\bin


## Running

**Executando o projeto**
**Nova estrutura**

Após abrir o terminal na pasta raiz do projeto, será executada é só executar o seguinte comando:

`cucumber`
 
**Tags**

Pode-se executar apenas cenários que contém tags específicas, testando assim funcionalidades isoladas ou testes isolados.

As tags disponíveis são:
* > @positivo
* > @negativo
* > @esquemaCenario
* > @login
* > @Cadastro
 

**Comandos para execução**

O comando `cucumber` recebe alguns argumentos como veremos abaixo. Caso não seja atribuído nenhum argumento o projeto inteiro será executado de ponta à ponta. Segue a lista de argumentos:

`cucumber @rerun.txt` Pode-se passar um arquivo de texto contendo o que será executado como nesse caso. Verificar formato rerun.

`-f, --format <formato_desejado>` Mudar o formato de saída do relatório, caso não especifique esse argumento será utilizado o formato padrão do projeto. Lista de formatos possíveis:
*  html        : Generates a nice looking HTML report.
*  json        : Prints the feature as JSON
*  json_pretty : Prints the feature as prettified JSON
*  junit       : Generates a report similar to Ant+JUnit.
*  pretty      : Prints the feature as is - in colours.
*  progress    : Prints one character per scenario.
*  rerun       : Prints failing files with line numbers.
*  stepdefs    : Prints All step definitions with their locations. Same as the usage formatter, except that steps are not printed.
*  summary     : Summary output of feature and scenarios
*  usage       : Prints where step definitions are used. The slowest step definitions (with duration) are listed first. If --dry-run is used the duration is not shown, and step definitions are sorted by filename instead.
*Também é possível criar e utlizar um formato customizado.*


`--init` Inicializa toda a estrutura de pastas e arquivos necessários para um projeto em cucumber.

`-o, --out <arquivo|diretório>` Usado para especificar que o relatório terá uma saída e o caminho dessa saída.

`-t, --tag @<nome_da_tag>` Executa apenas as features ou cenários que tiverem a tag especificada declarada. Por exemplo: `-t @positivo`. Além disso, podemos usar expressões lógicas como or e and, exemplo: `-t @positivo and @regressao`

`-n, --name <nome>` Executa apenas os elementos da feature que for igual a parte do nome especificado. Se for preenchido com mais de um nome, ele ira procurar por ambos, ou seja, pode ser usado para executar apenas um cenário, feature ou grupo de cenários que compartilham de nomes semelhantes por exemplo. `-n senha` isso irá executar tudo que contiver senha no nome.

`--fail-fast` Usado para parar a execução no primeiro cenário que falhar.

`--retry <tentativas>` Especifica a quantidade de vezes que ele irá executar os testes que falharam (Padrão: 0).

`-h, --help` Exibe ajuda.


**EXEMPLOS:**
#executa o projeto inteiro 
'$ cucumber`

#executa apenas as features marcadas com a tag @login
`$ cucumber -t @login` 



## Contributing Guide

Veja o documento de contribuição para o projeto [contributing-guide](./CONTRIBUTING.md).
